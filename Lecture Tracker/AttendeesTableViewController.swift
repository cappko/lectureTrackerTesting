//
//  AttendanceTableViewController.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import SystemConfiguration 

class AttendeesTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var attendeesTableView: UITableView!
    
    static var loadedAttendees: [Attendance] = []
    static var reloadTable: Bool = true
    
    override func viewDidAppear(_ animated: Bool)
    {
        if AttendeesTableViewController.reloadTable == true
        {
            DispatchQueue.main.async
            {
                self.attendeesTableView.reloadData()
            }
            AttendeesTableViewController.reloadTable = false
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.attendeesTableView.reloadData()
    }
    
    //TABLE VIEW INITIALIZATION
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return StudentsViewController.loadedStudents.count
    }
    
    /*func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
     {
     <#code#>
     }*/
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = attendeesTableView.dequeueReusableCell(withIdentifier: "AttendeeCell", for: indexPath) as! AttendeeTableViewCell
        
        for attendee in AttendeesTableViewController.loadedAttendees
        {
            if StudentsViewController.loadedStudents[indexPath.row].id == attendee.studentId && attendee.eventId == loadedEvent?.id
            {
                cell.attendeeSwitch.setOn(true, animated: false)
                break
            }
        }
        
        cell.indexPathRow = indexPath.row
        
        cell.attendeeNameLabel.text = StudentsViewController.loadedStudents[indexPath.row].name + " " + StudentsViewController.loadedStudents[indexPath.row].surname
        
        return cell
    }
    
    //EXIT THE VIEW
    @IBAction func backButton(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
}

