//
//  NewEventTableViewController.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import SystemConfiguration
import Realm
import RealmSwift

class NewEventTableViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextViewDelegate
{
    @IBOutlet weak var eventDayPicker: UIPickerView!
    @IBOutlet weak var eventRoomTextField: UITextField!
    @IBOutlet weak var eventTypeSegment: UISegmentedControl!
    @IBOutlet weak var eventStartTimePicker: UIDatePicker!
    @IBOutlet weak var eventEndTimePicker: UIDatePicker!
    
    var eventDayPickerData: [String] = ["Pondelok", "Utorok", "Streda", "Štvrtok", "Piatok", "Sobota", "Nedeľa"]
    var selectedRow: Int = 1
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        
    }
    
    @IBAction func createEvent(_ sender: Any)
    {
        if eventRoomTextField.text != nil && eventRoomTextField.text != ""
        {
            let eventDay = eventDayPickerData[selectedRow]
            let eventRoom = eventRoomTextField.text!
            var eventType = "Cvičenie"
            switch eventTypeSegment.selectedSegmentIndex
            {
            case 0:
                eventType = "Cvičenie"
            case 1:
                eventType = "Prednáška"
            default:
                break
            }
            
            let event = Event()
            
            event.userId = user.id
            event.day = eventDay
            event.room = eventRoom
            event.type = eventType
            event.timeOfStart = getHourFromDatePicker(datePicker: eventStartTimePicker)
            event.timeOfEnd = getHourFromDatePicker(datePicker: eventEndTimePicker)
            
            if DataProvider.sharedInstance.isInternetAvailable() == true
            {
                DataProvider.sharedInstance.postUserEvent(userId: user.id, subject_id: (loadedSubject?.id)!, day: event.day, room: event.room, type: event.type, timeOfStart: event.timeOfStart, timeOfEnd: event.timeOfEnd, completion:
                {
                    (result) in
                    if result == -1
                    {
                        let problemWithNewEventAlert = UIAlertController(title: "Nastal problém", message: "Vytvorenie udalosti sa nepodarilo", preferredStyle: .alert)
                        problemWithNewEventAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(problemWithNewEventAlert, animated: true, completion:
                        {
                            self.dismiss(animated: true, completion: nil)
                        })
                    }
                    else
                    {
                        DataProvider.sharedInstance.getUserEvents(userId: user.id, completion:
                        {
                            (result) in
                            //JUST GETTING EVENTS
                        })
                        
                        EventsViewController.reloadTable = true
                        self.dismiss(animated: true, completion: nil)
                    }
                })
            }
            else
            {
                let noInternetAlert = UIAlertController(title: "Chýba pripojenie!", message: "Pre vytvorenie udalosti sa prosím pripojte na internet!", preferredStyle: .alert)
                noInternetAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(noInternetAlert, animated: true, completion:
                {
                    self.dismiss(animated: true, completion: nil)
                })
            }
        }
        else
        {
            let notFilledDataAlert = UIAlertController(title: "Niektoré údaje ostali nevyplnené!", message: "Prosím vyplnte označenie miestnosti udalosti!", preferredStyle: .alert)
            notFilledDataAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(notFilledDataAlert, animated: true, completion: nil)
        }

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return false
    }
    
    //TIME FROM DATEPICKER IN STRING
    func getHourFromDatePicker(datePicker: UIDatePicker) -> String
    {
        let unitFlags = Set<Calendar.Component>([.hour, .minute])
        var calendar = Calendar.current
        //calendar.timeZone = TimeZone(identifier: "UTC")!
        let components = calendar.dateComponents(unitFlags, from: datePicker.date)
        
        return "\(components.hour!):\(components.minute!)"
    }
    
    
    //PICKER VIEW INITIALIZATION
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return eventDayPickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        selectedRow = row
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return eventDayPickerData[row]
    }

    
    //KEYBOARD EXIT
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.view.endEditing(true)
        self.tableView.endEditing(true)
    }
    
    //EXIT THE VIEW
    @IBAction func backButton(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    
}
