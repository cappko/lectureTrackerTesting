//
//  GradingPopoverViewController.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import RealmSwift

var gradingStudentName: String = ""
var gradingStudentId: Int = 0

var loadedEventId = 0
var loadedAssignment: Assignment? = nil

class GradingPopoverViewController: UIViewController
{
    @IBOutlet weak var studentNameLabel: UILabel!
    @IBOutlet weak var studentGradeTextField: UITextField!
    @IBOutlet weak var minMaxInfoLabel: UILabel!
    @IBOutlet weak var assignmentNameLabel: UILabel!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        studentNameLabel.text = gradingStudentName
        studentGradeTextField.text = "\((loadedAssignment?.studentGrade)!)"
        minMaxInfoLabel.text = "Body: minimum: \((loadedAssignment?.minPoints)!), maximum: \((loadedAssignment?.maxPoints)!)"
        assignmentNameLabel.text = (loadedAssignment?.name)!
    }
    
    @IBAction func saveAssignmentGrading(_ sender: Any)
    {
        if DataProvider.sharedInstance.isInternetAvailable() == true
        {
            if Int(studentGradeTextField.text!)! <= (loadedAssignment?.maxPoints)!
            {
                var studentPerformance = "..."
                var studentComment = "..."
                
                if (loadedAssignment?.studentComment)! != "" && (loadedAssignment?.studentComment)!.isEmpty == false
                {
                    studentComment = (loadedAssignment?.studentComment)!
                }
                
                if (loadedAssignment?.studentPerformance)! != "" && (loadedAssignment?.studentPerformance)!.isEmpty == false
                {
                    studentPerformance = (loadedAssignment?.studentPerformance)!
                }
                
                SwiftSpinner.show("Prebieha komunikácia so serverom")
                DataProvider.sharedInstance.postAssignment(assignment_id: (loadedAssignment?.id)!, event_id: loadedEventId, name: (loadedAssignment?.name)!, student_ids: [gradingStudentId], student_grade: Int(studentGradeTextField.text!)!, student_comment: studentComment, student_performance: studentPerformance, minpoints: (loadedAssignment?.minPoints)!, maxpoints: (loadedAssignment?.maxPoints)!, completion:
                {
                    (result) in
                    if result == -1
                    {
                        SwiftSpinner.hide()
                        let problemWithNewEventAlert = UIAlertController(title: "Nastal problém", message: "Hodnotenie sa nepodarilo", preferredStyle: .alert)
                        problemWithNewEventAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(problemWithNewEventAlert, animated: true, completion: nil)
                    }
                    
                    
                    for assignment in AssignmentsViewController.loadedAssignments
                    {
                        if assignment.id == loadedAssignment?.id
                        {
                            let realm = try! Realm()
                            
                            try! realm.write
                            {
                                assignment.studentGrade = Int(self.studentGradeTextField.text!)!
                            }
                        }
                    }
                    
                    SwiftSpinner.hide()
                    AssignmentsViewController.reloadTable = true
                    
                    self.dismiss(animated: true, completion: nil)
                })
            }
            else
            {
                let moreThanMaxPoints = UIAlertController(title: "Nesprávne hodnotenie!", message: "Nemožno udeliť viac bodov než je maximálne bodové ohodnotenie!", preferredStyle: .alert)
                moreThanMaxPoints.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(moreThanMaxPoints, animated: true, completion: nil)
            }
        }
        else
        {
            let noInternetAlert = UIAlertController(title: "Chýba pripojenie!", message: "Pre uloženie hodnotenia zadania sa prosím pripojte na internet!", preferredStyle: .alert)
            noInternetAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(noInternetAlert, animated: true, completion: nil)
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }


}
