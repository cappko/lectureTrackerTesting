//
//  Subject.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import Foundation
import RealmSwift


var user = User()

class User: Object
{
    dynamic var id = 0
    dynamic var email = ""
    dynamic var password = ""
    
    override static func primaryKey() -> String?
    {
        return "id"
    }

}


