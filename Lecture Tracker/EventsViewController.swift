//
//  EventsViewController.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import SystemConfiguration
import RealmSwift

var loadedSubject: Subject? = nil

class EventsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var eventsTableVIew: UITableView!
    
    static var loadedEvents: [Event] = []
    static var reloadTable: Bool = true
    
    override func viewDidAppear(_ animated: Bool)
    {
        if EventsViewController.reloadTable == true
        {
            DispatchQueue.main.async
            {
                self.eventsTableVIew.reloadData()
            }
            EventsViewController.reloadTable = false
        }
        EventsViewController.reloadTable = true
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.eventsTableVIew.reloadData()
    }
    
    //TABLE VIEW INITIALIZATION
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return EventsViewController.loadedEvents.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        loadedEvent = EventsViewController.loadedEvents[indexPath.row]
        self.performSegue(withIdentifier: "weekScreenSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! EventTableViewCell
        cell.eventTypeLabel.text = EventsViewController.loadedEvents[indexPath.row].type
        cell.eventRoomLabel.text = EventsViewController.loadedEvents[indexPath.row].room
        cell.eventTimeAndDayLabel.text = EventsViewController.loadedEvents[indexPath.row].day + ", " + EventsViewController.loadedEvents[indexPath.row].timeOfStart + "-" + EventsViewController.loadedEvents[indexPath.row].timeOfEnd
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            if isInternetAvailable() == true
            {
                DataProvider.sharedInstance.deleteUserEvent(eventId: EventsViewController.loadedEvents[indexPath.row].id, completion:
                {
                    (deleteResult) in
                    if deleteResult == true
                    {
                        let realm = try! Realm()
                        
                        try! realm.write
                        {
                            realm.delete(EventsViewController.loadedEvents[indexPath.row])
                        }
                        
                        EventsViewController.loadedEvents.remove(at: indexPath.row)
                        
                    
                        self.eventsTableVIew.deleteRows(at: [indexPath], with: .fade)
                    }
                    else
                    {
                        let noInternetAlert = UIAlertController(title: "Chyba!", message: "Vymazanie udalosti sa nepodarilo!", preferredStyle: .alert)
                        noInternetAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(noInternetAlert, animated: true, completion: nil)
                    }
                })
            }
            else
            {
                let noInternetAlert = UIAlertController(title: "Chýba pripojenie!", message: "Pre vymazanie udalosti sa prosím pripojte na internet!", preferredStyle: .alert)
                noInternetAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(noInternetAlert, animated: true, completion: nil)
            }
        }
    }
    
    //EXIT THE VIEW
    @IBAction func backButton(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    ////UTILS
    
    //CHECK INTERNET AVAILABILITY
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress)
        {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1)
            {
                zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags)
        {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
}
