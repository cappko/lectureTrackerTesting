//
//  Attendance.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import Foundation
import RealmSwift

class Attendance: Object
{
    dynamic var id = 0
    dynamic var eventId = 0
    dynamic var studentId = 0
    var attendance = List<Integer>()
    
    override static func primaryKey() -> String?
    {
        return "id"
    }
    
}
