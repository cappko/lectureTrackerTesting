//
//  UrlConfig.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import Foundation

public struct URLConfig
{
    struct Server
    {
        static let DEBUGBASE = "http://a6de2f2f.ngrok.io/lectureTracker"
        static let RELEASEBASE = "http://not yet determined"
        
        #if RELEASE
        static let BASEURL = RELEASEBASE
        #else
        static let BASEURL = DEBUGBASE
        #endif
    }
}
