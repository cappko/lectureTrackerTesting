//
//  LeftMenuViewController.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import RealmSwift

protocol LeftMenuDelegate
{
    func logout()
}

class LeftMenuViewController: UIViewController
{
    var leftMenuDelegate: LeftMenuDelegate?

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    
    @IBAction func studentButton(_ sender: Any)
    {
        if DataProvider.sharedInstance.isInternetAvailable() == true
        {
            DataProvider.sharedInstance.getUserStudents(userId: user.id, completion:
            {
                (result) in
                StudentsViewController.reloadTable = true
                self.performSegue(withIdentifier: "studentScreenSegue", sender: self)
            })
        }
        else
        {
            self.performSegue(withIdentifier: "studentScreenSegue", sender: self)
        }
    }
    
    @IBAction func gradingButton(_ sender: Any)
    {
        if DataProvider.sharedInstance.isInternetAvailable() == true
        {
            DataProvider.sharedInstance.getUserEvents(userId: user.id, completion:
            {
                (result) in
                GradingEventsViewController.reloadTable = true
                self.performSegue(withIdentifier: "gradingEventsScreenSegue", sender: self)
            })
        }
        else
        {
            self.performSegue(withIdentifier: "gradingEventsScreenSegue", sender: self)
        }
    }

    @IBAction func logoutButton(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

}
