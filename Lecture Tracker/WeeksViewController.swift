//
//  WeeksViewController.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import SystemConfiguration

var loadedEvent: Event? = nil

class WeeksViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var weeksTableView: UITableView!
    
    static var reloadTable: Bool = true
    
    override func viewDidAppear(_ animated: Bool)
    {
        if WeeksViewController.reloadTable == true
        {
            DispatchQueue.main.async
            {
                self.weeksTableView.reloadData()
            }
            WeeksViewController.reloadTable = false
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.weeksTableView.reloadData()
        
        AttendeesTableViewController.loadedAttendees.removeAll()
        DataProvider.sharedInstance.getStudentAttendanceByEventId(eventId: (loadedEvent?.id)!, completion:
        {
            (result) in
            //NOTHING TO DO HERE, COMPLETION IS IRRELEVANT
        })
    }
    
    //TABLE VIEW INITIALIZATION
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 13
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        StudentAttendanceTableViewController.weekNumber = indexPath.row
        self.performSegue(withIdentifier: "attendanceScreenSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        _ = UITableViewCell(style: .default, reuseIdentifier: "WeekCell")
        
        let cell = weeksTableView.dequeueReusableCell(withIdentifier: "WeekCell", for: indexPath)
        
        cell.textLabel?.text = String(indexPath.row + 1) + ". týždeň"
        
        return cell
    }
    
    @IBAction func attendeesSegueButton(_ sender: Any)
    {
        AttendeesTableViewController.loadedAttendees.removeAll()
        if DataProvider.sharedInstance.isInternetAvailable() == true
        {
            DataProvider.sharedInstance.getStudentAttendanceByEventId(eventId: (loadedEvent?.id)!, completion:
            {
                (result) in
                if result == true
                {
                    self.performSegue(withIdentifier: "attendeesScreenSegue", sender: self)
                }
            })
        }
        else
        {
            let attendanceByEventId = RealmProvider.sharedInstance.getAttendancesByEventId(eventId: (loadedEvent?.id)!)
            
            for attendance in attendanceByEventId
            {
                AttendeesTableViewController.loadedAttendees.append(attendance)
            }
        }
    }
    
    //EXIT THE VIEW
    @IBAction func backButton(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    ////UTILS
    
    //CHECK INTERNET AVAILABILITY
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress)
        {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1)
            {
                zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags)
        {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
}
