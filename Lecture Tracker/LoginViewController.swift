//
//  LoginViewController.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import LocalAuthentication

class LoginViewController: UIViewController, UITextFieldDelegate
{
    @IBOutlet weak var overlayView: UIVisualEffectView!
    
    @IBOutlet weak var loginEmailTextField: UITextField!
    @IBOutlet weak var loginPasswordTextField: UITextField!
    @IBOutlet weak var registerEmailTextField: UITextField!
    @IBOutlet weak var registerPasswordTextField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var touchIdButton: UIButton!
    
    let authenticationContext = LAContext()
    var error: NSError?
    var userName = ""
    var password = ""
    var loggingInFirst = true
    
    override func viewDidAppear(_ animated: Bool)
    {
        if overlayView.alpha == 1.0
        {
            overlayView.alpha = 0.0
            UIView.animate(withDuration: 1.0)
            {
                self.overlayView.alpha = 0.99
            }
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error)
        {
            
            //AUTHENTICATION - Fetch TouchID Setting
            let users = RealmProvider.sharedInstance.getUsers()
            
            if users.count > 0
            {
                for storedUser in users
                {
                    user = User()
                    user.email = storedUser.email
                    user.password = storedUser.password
                    user.id = storedUser.id
                }
                touchIdButton.isHidden = false
            }
            else
            {
                touchIdButton.isHidden = true
            }
        }
        

    }

    @IBAction func loginWithTouchId(_ sender: Any)
    {
        authenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Prosím verifikujte sa použitím Vášho Touch ID senzora!", reply:
        {
            (success, error) in
            if success
            {
                DispatchQueue.main.async
                {
                    if self.loggingInFirst == true
                    {
                        self.userName = user.email
                        self.password = user.password
                        self.loggingInFirst = false
                        self.loginEmailTextField.text = user.email
                        self.loginPasswordTextField.text = user.password
                        self.loginAction()
                    }
                    else
                    {
                        self.loginEmailTextField.text = self.userName
                        self.loginPasswordTextField.text = self.password
                        self.loginAction()
                    }
                }
            }
            else
            {
                
            }
        })

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        self.view.endEditing(true)
        return false
    }
    
    //LOG IN BUTTON
    @IBAction func loginUserAction(_ sender: Any)
    {
        loginAction()
    }
    
    func loginAction()
    {
        //CHECK IF INPUT ISN'T EMPTY
        if loginEmailTextField.text != nil && loginEmailTextField.text != "" && loginPasswordTextField.text != nil && loginPasswordTextField.text != ""
        {
            if (isValidEmail(loginEmailTextField.text!))
            {
                if DataProvider.sharedInstance.isInternetAvailable() == true
                {
                    SwiftSpinner.show("Prebieha prihlasovanie")
                    DataProvider.sharedInstance.postUserLogin(email: loginEmailTextField.text!, password: loginPasswordTextField.text!, completion:
                        {
                            (result) in
                            if result == true
                            {
                                SwiftSpinner.hide()
                                
                                
                                //LOGGED IN, SEGUE INTO THE USER AREA
                                self.performSegue(withIdentifier: "loginUserSegue", sender: self)
                            }
                            else if result == false
                            {
                                SwiftSpinner.hide()
                                //4xx or 5xx FROM THE SERVER, NOT SIGNED IN CORRECTLY
                                let wrongLoginAlert = UIAlertController(title: "Nesprávny kombinácia e-mailu a hesla", message: "Prosím, prihláste sa s existujúcimi prihlasovacími údajmi!", preferredStyle: .alert)
                                wrongLoginAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                                self.present(wrongLoginAlert, animated: true, completion:
                                    {
                                        self.loginPasswordTextField.text = ""
                                })
                            }
                    })
                }
                else
                {
                    DispatchQueue.main.async
                    {
                        let users = RealmProvider.sharedInstance.getUsers()
                        
                        if users.count > 0
                        {
                            var storedUser = User()
                            for user in users
                            {
                                storedUser = user
                            }
                            
                            if storedUser.email == self.loginEmailTextField.text && storedUser.password == self.loginPasswordTextField.text
                            {
                                if self.authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &self.error)
                                {
                                    self.touchIdButton.isHidden = false
                                }
                                
                                
                                //LOGGED IN, SEGUE INTO THE USER AREA
                                self.performSegue(withIdentifier: "loginUserSegue", sender: self)
                            }
                            else
                            {
                                let wrongLoginAlert = UIAlertController(title: "Nesprávny kombinácia e-mailu a hesla", message: "Prosím, prihláste sa s existujúcimi prihlasovacími údajmi!", preferredStyle: .alert)
                                wrongLoginAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                                self.present(wrongLoginAlert, animated: true, completion:
                                    {
                                        self.loginPasswordTextField.text = ""
                                })
                            }
                        }
                        else
                        {
                            let noInternetAlert = UIAlertController(title: "Chýba pripojenie!", message: "Pre prvé prihlásenie sa prosím pripojte na internet!", preferredStyle: .alert)
                            noInternetAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                            self.present(noInternetAlert, animated: true, completion:
                                {
                                    self.loginPasswordTextField.text = ""
                            })
                        }
                    }
                    
                }
            }
            else
            {
                let invalidEmailAlert = UIAlertController(title: "Nesprávny e-mail", message: "Prosím, prihláste sa s e-mailovou adresou v korektnom tvare!", preferredStyle: .alert)
                invalidEmailAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(invalidEmailAlert, animated: true, completion:
                    {
                        self.loginEmailTextField.text = ""
                        self.loginPasswordTextField.text = ""
                })
            }
        }
        else
        {
            let emptyValuesAlert = UIAlertController(title: "Jedna z hodnôt je prázdna", message: "Prosím vyplňte e-mail aj heslo zároveň pred prihlásením!", preferredStyle: .alert)
            emptyValuesAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(emptyValuesAlert, animated: true, completion:
                {
                    self.loginPasswordTextField.text = ""
            })
        }

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "loginUserSegue" && DataProvider.sharedInstance.isInternetAvailable()
        {
            if authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error)
            {
                
                //AUTHENTICATION - Fetch TouchID Setting
                touchIdButton.isHidden = false
            }

        }
    }
    
    @IBAction func registerUserAction(_ sender: Any)
    {
        //CHECK IF INPUT ISN'T EMPTY
        if registerEmailTextField.text != nil && registerEmailTextField.text != "" && registerPasswordTextField.text != nil && registerPasswordTextField.text != ""
        {
            if (isValidEmail(registerEmailTextField.text!))
            {
                if DataProvider.sharedInstance.isInternetAvailable() == true
                {
                    SwiftSpinner.show("Prebieha registrácia")
                    DataProvider.sharedInstance.postUserRegister(email: registerEmailTextField.text!, password: registerPasswordTextField.text!, completion:
                    {
                        (result) in
                        if result == "true"
                        {
                            DataProvider.sharedInstance.postUserLogin(email: self.registerEmailTextField.text!, password: self.registerPasswordTextField.text!, completion:
                            {
                                (result) in
                                if result == true
                                {
                                    SwiftSpinner.hide()
                                
                                    
                                    
                                    
                                    //REGISTERED AND LOGGED IN, SEGUE INTO THE USER AREA
                                    self.registerEmailTextField.text = ""
                                    self.registerPasswordTextField.text = ""
                                    
                                    if self.authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &self.error)
                                    {
                                        self.touchIdButton.isHidden = false
                                    }
                                    
                                    self.performSegue(withIdentifier: "loginUserSegue", sender: self)
                                }
                                else
                                {
                                    SwiftSpinner.hide()
                                }
                            })
                        }
                        else if result == "exists"
                        {
                            SwiftSpinner.hide()
                            //A USER WITH THIS EMAIL ALREADY EXISTS, POP UP MESSAGE TO REGISTER AGAIN
                            let userAlreadyRegisteredAlert = UIAlertController(title: "Užívateľ už existuje!", message: "Registrujte sa s unikátnym prihlasovacím e-mailom!", preferredStyle: .alert)
                            userAlreadyRegisteredAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                            self.present(userAlreadyRegisteredAlert, animated: true, completion:
                            {
                                self.registerEmailTextField.text = ""
                                self.registerPasswordTextField.text = ""
                            })
                        }
                        else if result == "false"
                        {
                            //SOME OTHER ERROR ON SERVER, POP UP MESSAGE TO REGISTER LATER
                            let wrongLoginAlert = UIAlertController(title: "Chyba!", message: "Pri registrácii nastala chyba na serveri, skúste to neskôr prosím!", preferredStyle: .alert)
                            wrongLoginAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                            self.present(wrongLoginAlert, animated: true, completion:
                            {
                                self.registerEmailTextField.text = ""
                                self.registerPasswordTextField.text = ""
                            })
                        }
                    })
                }
                else
                {
                    let noInternetAlert = UIAlertController(title: "Chýba pripojenie!", message: "Pre registráciu sa prosím pripojte na internet!", preferredStyle: .alert)
                    noInternetAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(noInternetAlert, animated: true, completion: nil)
                }
            }
            else
            {
                let invalidEmailAlert = UIAlertController(title: "Nesprávny e-mail", message: "Prosím, registrujte sa s e-mailovou adresou v korektnom tvare!", preferredStyle: .alert)
                invalidEmailAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(invalidEmailAlert, animated: true, completion: nil)
            }
        }
        else
        {
            let emptyValuesAlert = UIAlertController(title: "Jedna z hodnôt je prázdna", message: "Prosím vyplňte e-mail aj heslo zároveň pred registráciou!", preferredStyle: .alert)
            emptyValuesAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(emptyValuesAlert, animated: true, completion: nil)
        }

    }
    
    
    
    ////UTILS
    
    //REGEX FOR EMAIL VALIDITY
    func isValidEmail(_ testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

    //KEYBOARD EXIT
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.view.endEditing(true)
    }

}
