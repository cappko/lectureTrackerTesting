//
//  Assignment.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import Foundation
import RealmSwift

class Assignment: Object
{
    dynamic var id = 0
    dynamic var eventId = 0
    dynamic var name = ""
    dynamic var studentId = 0
    dynamic var studentGrade = 0
    dynamic var studentComment = ""
    dynamic var studentPerformance = ""
    dynamic var minPoints = 0
    dynamic var maxPoints = 0
    
    override static func primaryKey() -> String?
    {
        return "id"
    }

}

