//
//  NewStudentTableViewController.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import SystemConfiguration
import RealmSwift
import Realm

class NewStudentTableViewController: UITableViewController
{
    @IBOutlet weak var studentNameTextField: UITextField!
    @IBOutlet weak var studentSurnameTextField: UITextField!
    @IBOutlet weak var studentEmailTextField: UITextField!
    @IBOutlet weak var studentISICTextField: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    @IBAction func createStudent(_ sender: Any)
    {
        if studentNameTextField.text != nil && studentNameTextField.text != "" && studentSurnameTextField.text != nil && studentSurnameTextField.text != "" && studentEmailTextField.text != "" && studentEmailTextField.text != nil && studentISICTextField.text != "" && studentISICTextField.text != nil
        {
            let studentName = studentNameTextField.text!
            let studentSurname = studentSurnameTextField.text!
            let studentEmail = studentEmailTextField.text!
            let studentISIC = studentISICTextField.text!
            
            if isValidEmail(studentEmail) == true
            {
                let newStudent = Student()
                newStudent.name = studentName
                newStudent.surname = studentSurname
                newStudent.email = studentEmail
                newStudent.isicNumber = studentISIC
                
                if isInternetAvailable() == true
                {
                    SwiftSpinner.show("Komunikácia so serverom")
                    DataProvider.sharedInstance.postUserStudent(userId: user.id, name: newStudent.name, surname: newStudent.surname, email: newStudent.email, isicnumber: newStudent.isicNumber, completion:
                    {
                        (result) in
                        if result == -1
                        {
                            SwiftSpinner.hide()
                            let problemWithNewSubjectAlert = UIAlertController(title: "Nastal problém", message: "Vytvorenie študenta sa nepodarilo", preferredStyle: .alert)
                            problemWithNewSubjectAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                            self.present(problemWithNewSubjectAlert, animated: true, completion:
                            {
                                self.dismiss(animated: true, completion: nil)
                            })
                        }
                        else
                        {
                            SwiftSpinner.hide()
                            newStudent.id = result!
                            StudentsViewController.loadedStudents.append(newStudent)
                            let realm = try! Realm()
                            
                            try! realm.write
                            {
                                realm.add(newStudent, update: true)
                            }
                            StudentsViewController.reloadTable = true
                            
                            self.dismiss(animated: true, completion: nil)
                        }
                    })
                }
                else
                {
                    let noInternetAlert = UIAlertController(title: "Chýba pripojenie!", message: "Pre pridanie študenta sa prosím pripojte na internet!", preferredStyle: .alert)
                    noInternetAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(noInternetAlert, animated: true, completion:
                    {
                        self.dismiss(animated: true, completion: nil)
                    })
                }
                
            }
            else
            {
                let invalidEmailAlert = UIAlertController(title: "Nesprávny e-mail", message: "Prosím, vyplnte e-mailovú adresu študenta v korektnom tvare!", preferredStyle: .alert)
                invalidEmailAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(invalidEmailAlert, animated: true, completion: nil)
            }
        }
        else
        {
            let notFilledDataAlert = UIAlertController(title: "Niektoré údaje ostali nevyplnené!", message: "Prosím vyplnte korektne všetky údaje o študentovi!", preferredStyle: .alert)
            notFilledDataAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(notFilledDataAlert, animated: true, completion: nil)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.view.endEditing(true)
    }
    
    
    //EXIT THE VIEW
    @IBAction func backButton(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    ////UTILS
    
    //REGEX FOR EMAIL VALIDITY
    func isValidEmail(_ testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    //CHECK INTERNET AVAILABILITY
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress)
        {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1)
            {
                zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags)
        {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }


}
