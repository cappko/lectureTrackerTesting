//
//  EventsViewController.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import SystemConfiguration

class AssignmentsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPopoverPresentationControllerDelegate
{
    @IBOutlet weak var assignmentsTableView: UITableView!
    @IBOutlet weak var popoverButton: UIButton!
    
    static var loadedAssignments: [Assignment] = []
    static var reloadTable: Bool = true
    
    override func viewDidAppear(_ animated: Bool)
    {
        reloadTableHandler()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.assignmentsTableView.reloadData()
        
        _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(AssignmentsViewController.reloadTableHandler), userInfo: nil, repeats: true)
    }
    
    //TABLE VIEW INITIALIZATION
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return AssignmentsViewController.loadedAssignments.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if DataProvider.sharedInstance.isInternetAvailable() == true
        {
            DataProvider.sharedInstance.getUserEvents(userId: user.id, completion:
            {
                (result) in
                if result == true
                {
                    self.popoverButton.center = CGPoint(x: self.popoverButton.center.x, y: 28 + (57 * CGFloat(indexPath.row)))
                    
                    for student in StudentsViewController.loadedStudents
                    {
                        if student.id == AssignmentsViewController.loadedAssignments[indexPath.row].studentId
                        {
                            gradingStudentName = student.name + " " + student.surname
                            gradingStudentId = student.id
                            
                            loadedAssignment = AssignmentsViewController.loadedAssignments[indexPath.row]
                        }
                    }
                    
                    self.performSegue(withIdentifier: "gradingPopover", sender: self)
                }
            })
        }
        else
        {
            let noInternetAlert = UIAlertController(title: "Chýba pripojenie!", message: "Pre hodnotenie sa prosím pripojte na internet!", preferredStyle: .alert)
            noInternetAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(noInternetAlert, animated: true, completion: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "gradingPopover"
        {
            let popoverViewController = segue.destination
            popoverViewController.preferredContentSize = CGSize(width: self.view.frame.width, height: 300)
            popoverViewController.popoverPresentationController?.delegate = self
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle
    {
        return .none
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "AssignmentCell")
        
        var studentName = ""
        
        for student in StudentsViewController.loadedStudents
        {
            if student.id == AssignmentsViewController.loadedAssignments[indexPath.row].studentId
            {
                studentName = student.name + " " + student.surname
            }
        }
        
        cell.textLabel?.text = AssignmentsViewController.loadedAssignments[indexPath.row].name + ": " + studentName
        cell.textLabel?.textColor = UIColor.white
        
        let currentGrade = AssignmentsViewController.loadedAssignments[indexPath.row].studentGrade
        let minGrade = AssignmentsViewController.loadedAssignments[indexPath.row].minPoints
        let maxGrade = AssignmentsViewController.loadedAssignments[indexPath.row].maxPoints
        
        if currentGrade <= minGrade
        {
            cell.backgroundColor = UIColor.red
        }
        else if currentGrade > minGrade && currentGrade < (maxGrade - (Int(((maxGrade - minGrade) / 2))))
        {
            cell.backgroundColor = UIColor.blue
        }
        else
        {
            cell.backgroundColor = UIColor.green
        }
        
        cell.separatorInset = .init()
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            if DataProvider.sharedInstance.isInternetAvailable() == true
            {
                DataProvider.sharedInstance.deleteUserAssignment(assignmentId: AssignmentsViewController.loadedAssignments[indexPath.row].id, completion:
                {
                    (deleteResult) in
                    if deleteResult == true
                    {
                        AssignmentsViewController.loadedAssignments.remove(at: indexPath.row)
                        self.assignmentsTableView.deleteRows(at: [indexPath], with: .fade)
                    }
                    else
                    {
                        let noInternetAlert = UIAlertController(title: "Chyba!", message: "Vymazanie udalosti sa nepodarilo!", preferredStyle: .alert)
                        noInternetAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(noInternetAlert, animated: true, completion: nil)
                    }
                })
            }
            else
            {
                let noInternetAlert = UIAlertController(title: "Chýba pripojenie!", message: "Pre vymazanie zadania sa prosím pripojte na internet!", preferredStyle: .alert)
                noInternetAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(noInternetAlert, animated: true, completion: nil)
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.view.endEditing(true)
    }
    
    func reloadTableHandler()
    {
        if AssignmentsViewController.reloadTable == true
        {
            DispatchQueue.main.async
            {
                self.assignmentsTableView.reloadData()
            }
            AssignmentsViewController.reloadTable = false
        }
        AssignmentsViewController.reloadTable = true
    }
    
    //EXIT THE VIEW
    @IBAction func backButton(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
}

