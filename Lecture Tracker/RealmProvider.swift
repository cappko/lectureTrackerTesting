//
//  RealmProvider.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class RealmProvider
{
    var realm = try! Realm()
    
    init() {}
    
    //SINGLE INSTANCE FOR THE WHOLE APP
    class var sharedInstance: RealmProvider
    {
        struct Singleton
        {
            static let instance = RealmProvider()
        }
        return Singleton.instance
    }
    
    //USERS
    func getUsers() -> Results<User>
    {
        return realm.objects(User.self)
    }
    
    func storeUser(user: User)
    {
        try! self.realm.write
        {
            self.realm.add(user, update: true)
        }
    }
    
    func deleteUsers()
    {
        let users = getUsers()
        try! self.realm.write
        {
            self.realm.delete(users)
        }
    }
    
    //SUBJECTS
    func getSubjects() -> Results<Subject>
    {
        return realm.objects(Subject.self)
    }
    
    func storeSubject(subject: Subject)
    {
        try! self.realm.write
        {
            realm.add(subject, update: true)
        }
    }

    
    func deleteSubjects()
    {
        let subjectsFromDb = self.getSubjects()
        
        for subject in subjectsFromDb
        {
            try! realm.write
            {
                realm.delete(subject)
            }
        }
    }
    
    //STUDENTS
    func getStudents() -> Results<Student>
    {
        return realm.objects(Student.self)
    }
    
    func storeStudent(student: Student)
    {
        try! self.realm.write
        {
            realm.add(student, update: true)
        }
    }
    

    
    func deleteStudents()
    {
        let studentsFromDb = self.getStudents()
        
        for student in studentsFromDb
        {
            try! realm.write
            {
                realm.delete(student)
            }
        }
    }
    
    //EVENTS
    func getEvents() -> Results<Event>
    {
        return realm.objects(Event.self)
    }
    
    func storeEvent(event: Event)
    {
        try! self.realm.write
        {
            realm.add(event, update: true)
        }
    }
    

    
    func deleteEvents()
    {
        let eventsFromDb = self.getEvents()
        
        for event in eventsFromDb
        {
            try! realm.write
            {
                realm.delete(event)
            }
        }
    }
    
    //ATTENDANCES
    func getAttendancesByEventId(eventId: Int) -> Results<Attendance>
    {
        return realm.objects(Attendance.self).filter("eventId = \(eventId)")
    }
    
    func storeAttendance(attendance: Attendance)
    {
        try! self.realm.write
        {
            realm.add(attendance, update: true)
        }
    }
    

    
    
    /*//ASSIGNMENTS
    func getAssignments() -> Results<Assignment>
    {
        return realm.objects(Assignment.self)
    }
    
    func storeAssignment(assignment: Assignment)
    {
        try! self.realm.write
        {
            realm.add(assignment, update: true)
        }
    }
    

    
    func deleteAssignments()
    {
        let assignmentsFromDb = self.getAssignments()
        
        for assignment in assignmentsFromDb
        {
            try! realm.write
            {
                realm.delete(assignment)
            }
        }
    }*/

    
    
}
