//
//  AttendanceTableViewController.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import SystemConfiguration

class StudentAttendanceTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var studentAttendanceTableView: UITableView!
    
    static var reloadTable: Bool = true
    static var weekNumber: Int = 0
    
    override func viewDidAppear(_ animated: Bool)
    {
        if StudentAttendanceTableViewController.reloadTable == true
        {
            DispatchQueue.main.async
            {
                self.studentAttendanceTableView.reloadData()
            }
            StudentAttendanceTableViewController.reloadTable = false
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.studentAttendanceTableView.reloadData()
    }
    
    //TABLE VIEW INITIALIZATION
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return AttendeesTableViewController.loadedAttendees.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 168.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = studentAttendanceTableView.dequeueReusableCell(withIdentifier: "StudentAttendanceCell", for: indexPath) as! StudentAttendanceTableViewCell
    
        let attendance = AttendeesTableViewController.loadedAttendees[indexPath.row].attendance
        var attendanceNumbers: [Int] = []
        
        print(attendance.first!.val)
        for attendanceRealmNumber in attendance
        {
            attendanceNumbers.append(attendanceRealmNumber.val)
        }
        
        print("NEW LOOP:")
        
        for attendanceRealmNumber in attendance
        {
            print(attendanceRealmNumber.val)
        }
        
        if attendanceNumbers[StudentAttendanceTableViewController.weekNumber] == 1
        {
            cell.studentAttendingSwitch.setOn(true, animated: false)
        }
        else
        {
            cell.studentAttendingSwitch.setOn(false, animated: false)
        }
        
        cell.indexPathRow = indexPath.row
        cell.studentId = AttendeesTableViewController.loadedAttendees[indexPath.row].studentId
        cell.attendanceId = AttendeesTableViewController.loadedAttendees[indexPath.row].id
        
        var studentNameAndSurname: String = ""
        for student in StudentsViewController.loadedStudents
        {
            if student.id == AttendeesTableViewController.loadedAttendees[indexPath.row].studentId
            {
                studentNameAndSurname = student.name + " " + student.surname
                break
            }
        }
        
        cell.studentNameAndSurnameLabel.text = studentNameAndSurname
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    //EXIT THE VIEW
    @IBAction func backButton(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
}
