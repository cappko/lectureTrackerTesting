//
//  StudentAttendanceTableViewCell.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import RealmSwift

class StudentAttendanceTableViewCell: UITableViewCell
{
    @IBOutlet weak var studentNameAndSurnameLabel: UILabel!
    @IBOutlet weak var studentAttendingSwitch: UISwitch!
    @IBOutlet weak var studentTextField: UITextView!
    
    let realm = try! Realm()
    

    var studentId: Int = 0
    var indexPathRow: Int = 0
    var attendanceId: Int = 0
    var editingText: Bool = false
    
    @IBAction func switchedAttendance(_ sender: Any)
    {
        if DataProvider.sharedInstance.isInternetAvailable() == false
        {
            reverseSwitch()
        }
        else if studentAttendingSwitch.isOn == true
        {
            var attendancesRealm = AttendeesTableViewController.loadedAttendees[indexPathRow].attendance
            
            var attendance: [Int] = []
            
            for attendanceRealm in attendancesRealm
            {
                attendance.append(attendanceRealm.val)
            }
            
            for i in 0..<13
            {
                if i == StudentAttendanceTableViewController.weekNumber
                {
                    attendance[i] = 1
                }
            }
            
            try! realm.write
            {
                attendancesRealm.removeAll()
                
                for attendanceNumber in attendance
                {
                    let integer = Integer()
                    integer.val = attendanceNumber
                    attendancesRealm.append(integer)
                }
            }

            
            
            try! realm.write
            {
                AttendeesTableViewController.loadedAttendees[indexPathRow].attendance = attendancesRealm
            }
            DataProvider.sharedInstance.postAttendance(attendance_id: attendanceId, student_id: studentId, event_id: (loadedEvent?.id)!, attendance: attendance, completion:
            {
                (attendanceId) in
                if attendanceId == -1
                {
                    self.reverseSwitch()
                }
            })
        }
        else if studentAttendingSwitch.isOn == false
        {
            let attendancesRealm = AttendeesTableViewController.loadedAttendees[indexPathRow].attendance
            
            var attendance: [Int] = []
            
            for attendanceRealm in attendancesRealm
            {
                attendance.append(attendanceRealm.val)
            }
            
            for i in 0..<13
            {
                if i == StudentAttendanceTableViewController.weekNumber
                {
                    attendance[i] = 0
                }
            }
            
            try! realm.write
            {
                attendancesRealm.removeAll()
                for attendanceNumber in attendance
                {
                    let integer = Integer()
                    integer.val = attendanceNumber
                    attendancesRealm.append(integer)
                }
            }
            
            
           
            try! realm.write
            {
                AttendeesTableViewController.loadedAttendees[indexPathRow].attendance = attendancesRealm
            }
            
            DataProvider.sharedInstance.postAttendance(attendance_id: attendanceId, student_id: studentId, event_id: (loadedEvent?.id)!, attendance: attendance, completion:
            {
                (attendanceId) in
                if attendanceId == -1
                {
                    self.reverseSwitch()
                }
            })
        }
    }
    
    /*override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        if studentNoteTextBox.isFirstResponder
        {
            editingText = true
        }
        else
        {
            editingText = false
        }
    }*/
    
    @IBAction func noteEdited(_ sender: Any)
    {
        print("FUKYEA")
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.endEditing(true)
    }
    
    
    func reverseSwitch()
    {
        if studentAttendingSwitch.isOn == false
        {
            studentAttendingSwitch.setOn(true, animated: true)
        }
        else
        {
            studentAttendingSwitch.setOn(false, animated: true)
        }
    }

    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

    
}
