//
//  StudentsViewController.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import SystemConfiguration
import RealmSwift

class StudentsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var studentsTableView: UITableView!

    static var loadedStudents: [Student] = []
    static var reloadTable: Bool = false
    
    override func viewDidAppear(_ animated: Bool)
    {
        if StudentsViewController.reloadTable == true
        {
            DispatchQueue.main.async
            {
                self.studentsTableView.reloadData()
            }
            StudentsViewController.reloadTable = false
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }

    //TABLEVIEW INITIALIZATION
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return StudentsViewController.loadedStudents.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 148.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = studentsTableView.dequeueReusableCell(withIdentifier: "StudentCell", for: indexPath) as! StudentTableViewCell
        cell.studentNameLabel.text = StudentsViewController.loadedStudents[indexPath.row].name + " " + StudentsViewController.loadedStudents[indexPath.row].surname
        cell.studentEmailLabel.text = StudentsViewController.loadedStudents[indexPath.row].email
        cell.studentISICLabel.text = StudentsViewController.loadedStudents[indexPath.row].isicNumber
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            if isInternetAvailable() == true
            {
                DataProvider.sharedInstance.deleteUserStudent(studentId: StudentsViewController.loadedStudents[indexPath.row].id, completion:
                {
                    (result) in
                    if result == true
                    {
                        let realm = try! Realm()
                        
                        try! realm.write
                        {
                            realm.delete(StudentsViewController.loadedStudents[indexPath.row])
                        }
                        
                        StudentsViewController.loadedStudents.remove(at: indexPath.row)
                        
                        self.studentsTableView.deleteRows(at: [indexPath], with: .fade)
                    }
                    else
                    {
                        let noInternetAlert = UIAlertController(title: "Chyba!", message: "Vymazanie študenta sa nepodarilo!", preferredStyle: .alert)
                        noInternetAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(noInternetAlert, animated: true, completion: nil)
                    }
                })
            }
            else
            {
                let noInternetAlert = UIAlertController(title: "Chýba pripojenie!", message: "Pre vytvorenie predmetu sa prosím pripojte na internet!", preferredStyle: .alert)
                noInternetAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(noInternetAlert, animated: true, completion: nil)
            }
        }
    }
    
    ////UTILS
    
    //CHECK INTERNET AVAILABILITY
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress)
        {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1)
            {
                zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags)
        {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //EXIT THE VIEW
    @IBAction func backButton(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    


}
