//
//  Subject.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import Foundation
import RealmSwift

class Subject: Object
{
    dynamic var id = 0
    dynamic var userId = 0
    dynamic var name = ""
    
    override static func primaryKey() -> String?
    {
        return "id"
    }

}


