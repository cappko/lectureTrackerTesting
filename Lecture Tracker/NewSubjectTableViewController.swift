//
//  NewSubjectTableViewController.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import SystemConfiguration

class NewSubjectTableViewController: UITableViewController
{
    @IBOutlet weak var subjectNameTextField: UITextField!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func createSubject(_ sender: Any)
    {
        if subjectNameTextField.text != nil && subjectNameTextField.text != ""
        {
            let subjectName = subjectNameTextField.text!
            
            let subject = Subject()
            
            subject.name = subjectName
            
            if DataProvider.sharedInstance.isInternetAvailable() == true
            {
                DataProvider.sharedInstance.postUserSubject(userId: user.id, name: subject.name, completion:
                    {
                        (result) in
                        if result == -1
                        {
                            let problemWithNewSubjectAlert = UIAlertController(title: "Nastal problém", message: "Vytvorenie predmetu sa nepodarilo", preferredStyle: .alert)
                            problemWithNewSubjectAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                            self.present(problemWithNewSubjectAlert, animated: true, completion:
                                {
                                    self.dismiss(animated: true, completion: nil)
                            })
                        }
                        else
                        {
                            subject.id = result!
                            SubjectsViewController.loadedSubjects.append(subject)
                            SubjectsViewController.reloadTable = true
                            
                            self.dismiss(animated: true, completion: nil)
                        }
                })
            }
            else
            {
                let noInternetAlert = UIAlertController(title: "Chýba pripojenie!", message: "Pre vytvorenie predmetu sa prosím pripojte na internet!", preferredStyle: .alert)
                noInternetAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(noInternetAlert, animated: true, completion:
                    {
                        self.dismiss(animated: true, completion: nil)
                })
            }
        }
        else
        {
            let notFilledDataAlert = UIAlertController(title: "Niektoré údaje ostali nevyplnené!", message: "Prosím vyplnte názov predmetu!", preferredStyle: .alert)
            notFilledDataAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(notFilledDataAlert, animated: true, completion: nil)
        }
    }
    

    //KEYBOARD EXIT
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        self.view.endEditing(true)
        self.tableView.endEditing(true)
    }
    
    //EXIT THE VIEW
    @IBAction func backButton(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

}
