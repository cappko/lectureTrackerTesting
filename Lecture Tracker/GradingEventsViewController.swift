//
//  EventsViewController.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import SystemConfiguration

class GradingEventsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet weak var eventsTableVIew: UITableView!

    static var reloadTable: Bool = true
    
    override func viewDidAppear(_ animated: Bool)
    {
        if GradingEventsViewController.reloadTable == true
        {
            DispatchQueue.main.async
            {
                self.eventsTableVIew.reloadData()
            }
            GradingEventsViewController.reloadTable = false
        }
        GradingEventsViewController.reloadTable = true
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.eventsTableVIew.reloadData()
    }
    
    //TABLE VIEW INITIALIZATION
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return allUserEvents.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        loadedEvent = allUserEvents[indexPath.row]
        if DataProvider.sharedInstance.isInternetAvailable() == true
        {
            DataProvider.sharedInstance.getEventAssignments(eventId: (loadedEvent?.id)!, completion:
            {
                (result) in
                if result == true
                {
                    DataProvider.sharedInstance.getStudentAttendanceByEventId(eventId: (loadedEvent?.id)!, completion:
                    {
                        (result) in
                        if result == true
                        {
                            loadedEventId = (loadedEvent?.id)!
                            self.performSegue(withIdentifier: "assignmentsSegue", sender: self)
                        }
                    })
                }
            })
        }
        else
        {
            self.performSegue(withIdentifier: "assignmentsSegue", sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 120.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as! EventTableViewCell
        cell.eventRoomLabel.text = allUserEvents[indexPath.row].type
        
        for subject in SubjectsViewController.loadedSubjects
        {
            if subject.id == allUserEvents[indexPath.row].subjectId
            {
                cell.eventTypeLabel.text = subject.name
                break
            }
        }
        
        cell.eventTimeAndDayLabel.text = allUserEvents[indexPath.row].day + ", " + allUserEvents[indexPath.row].timeOfStart + "-" + allUserEvents[indexPath.row].timeOfEnd
        
        return cell
    }
    
    //EXIT THE VIEW
    @IBAction func backButton(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
}
