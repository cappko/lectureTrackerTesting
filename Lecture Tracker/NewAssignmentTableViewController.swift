//
//  NewAssignmentTableViewController.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import SystemConfiguration

class NewAssignmentTableViewController: UITableViewController
{
    @IBOutlet weak var assignmentNameLabel: UITextField!
    @IBOutlet weak var minPointsLabel: UITextField!
    @IBOutlet weak var maxPointsLabel: UITextField!

    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    @IBAction func createAssignment(_ sender: Any)
    {
        if assignmentNameLabel.text != nil && assignmentNameLabel.text != "" && minPointsLabel.text != nil && minPointsLabel.text != "" && maxPointsLabel.text != nil && maxPointsLabel.text != ""
        {
            if DataProvider.sharedInstance.isInternetAvailable() == true
            {
                var studentIds: [Int] = []
                var studentGrades: [Int] = []
                
                for attendant in AttendeesTableViewController.loadedAttendees
                {
                    studentIds.append(attendant.studentId)
                    studentGrades.append(0)
                }
                
                if studentIds.count == 0
                {
                    let noStudentsAlert = UIAlertController(title: "Chyba!", message: "Pre vytvorenie zadania musí mať daná udalosť priradených študentov!", preferredStyle: .alert)
                    noStudentsAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                    self.present(noStudentsAlert, animated: true, completion: nil)
                }
                else
                {
                    SwiftSpinner.show("Prebieha komunikácia so serverom")
                    DataProvider.sharedInstance.postAssignment(assignment_id: 1, event_id: (loadedEvent?.id)!, name: assignmentNameLabel.text!, student_ids: studentIds, student_grade: 1, student_comment: "something", student_performance: "lenivec", minpoints: Int(minPointsLabel.text!)!, maxpoints: Int(maxPointsLabel.text!)!, completion:
                    {
                        (result) in
                        if result == -1
                        {
                            SwiftSpinner.hide()
                            let problemWithNewEventAlert = UIAlertController(title: "Nastal problém", message: "Vytvorenie zadania sa nepodarilo", preferredStyle: .alert)
                            problemWithNewEventAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                            self.present(problemWithNewEventAlert, animated: true, completion:
                            {
                                self.dismiss(animated: true, completion: nil)
                            })
                        }
                        else
                        {
                            SwiftSpinner.hide()
                            AssignmentsViewController.loadedAssignments.removeAll()
                            
                            DataProvider.sharedInstance.getEventAssignments(eventId: (loadedEvent?.id)!, completion:
                            {
                                (result) in
                                if result == true
                                {
                                    self.dismiss(animated: true, completion: nil)
                                }
                            })
                            AssignmentsViewController.reloadTable = true
                        }
                    })
                }
            }
            else
            {
                let noInternetAlert = UIAlertController(title: "Chýba pripojenie!", message: "Pre vytvorenie zadania sa prosím pripojte na internet!", preferredStyle: .alert)
                noInternetAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(noInternetAlert, animated: true, completion:
                    {
                        self.dismiss(animated: true, completion: nil)
                })
            }
        }
        else
        {
            let notFilledDataAlert = UIAlertController(title: "Niektoré údaje ostali nevyplnené!", message: "Prosím vyplnte všetky potrebné údaje o zadaní!", preferredStyle: .alert)
            notFilledDataAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(notFilledDataAlert, animated: true, completion: nil)
        }

    }
    

    //EXIT THE VIEW
    @IBAction func backButton(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

}

