//
//  Integer.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import Foundation
import RealmSwift

//THIS NEEDS TO BE DONE BECAUSE REALM CAN'T REALLY DO LISTS OF PRIMITIVES
public class Integer: Object
{
    dynamic var val: Int = 0
}
