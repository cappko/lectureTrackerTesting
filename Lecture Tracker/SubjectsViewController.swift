//
//  SubjectsViewController.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import SystemConfiguration

class SubjectsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, LeftMenuDelegate
{
    @IBOutlet weak var leftMenu: UIVisualEffectView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var tableView: UIView!
    @IBOutlet weak var subjectsTableVIew: UITableView!
    
    //LEFT MENU VARIABLES
    var frontViews: [UIView] = []
    var defaultViewPosition: CGFloat = 0.0
    
    static var loadedSubjects: [Subject] = []
    static var reloadTable: Bool = true
    
    override func viewDidAppear(_ animated: Bool)
    {
        if SubjectsViewController.reloadTable == true
        {
            DispatchQueue.main.async
            {
                self.subjectsTableVIew.reloadData()
            }
            SubjectsViewController.reloadTable = false
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        SubjectsViewController.loadedSubjects.removeAll()
        
        StudentsViewController.loadedStudents.removeAll()
        if DataProvider.sharedInstance.isInternetAvailable() == true
        {
            DataProvider.sharedInstance.getUserSubjects(userId: user.id, completion:
            {
                (result) in
                if result == true
                {
                    self.subjectsTableVIew.reloadData()
                }
            })
            
            let students = RealmProvider.sharedInstance.getStudents()
            
            if students.count > 0
            {
                for student in students
                {
                    try! RealmProvider.sharedInstance.realm.write
                    {
                        RealmProvider.sharedInstance.realm.delete(student)
                    }
                }
            }
            DataProvider.sharedInstance.getUserStudents(userId: user.id, completion:
            {
                (result) in
                //NOTHING TO DO HERE, JUST DOWNLOADS THE STUDENTS
            })
            
            let events = RealmProvider.sharedInstance.getEvents()
            
            if events.count > 0
            {
                for event in events
                {
                    try! RealmProvider.sharedInstance.realm.write
                    {
                        RealmProvider.sharedInstance.realm.delete(event)
                    }
                }
            }
            DataProvider.sharedInstance.getUserEvents(userId: user.id)
            {
                (result) in
                //NOTHING TO DO HERE, JUST DOWNLOADS THE EVENTS
            }
        }
        else
        {
            let students = RealmProvider.sharedInstance.getStudents()
            
            if students.count > 0
            {
                for student in students
                {
                    StudentsViewController.loadedStudents.append(student)
                }
            }
            
            let subjects = RealmProvider.sharedInstance.getSubjects()
            
            if subjects.count > 0
            {
                for subject in subjects
                {
                    SubjectsViewController.loadedSubjects.append(subject)
                }
                subjectsTableVIew.reloadData()
            }
            
            let events = RealmProvider.sharedInstance.getEvents()
            
            if events.count > 0
            {
                for event in events
                {
                    allUserEvents.append(event)
                }
            }
            
            
            
            
        }
        
        //LEFT MENU INITIALIZATION
        defaultViewPosition = self.view.center.x
        frontViews.append(navigationView)
        frontViews.append(tableView)
        
        let menu = storyboard!.instantiateViewController(withIdentifier: "LeftMenu") as! LeftMenuViewController
        addChildViewController(menu)
        menu.view.frame = self.leftMenu.frame
        self.leftMenu.addSubview(menu.view)
        menu.didMove(toParentViewController: self)
        //menu.leftMenuDelegate = self
        
        self.subjectsTableVIew.reloadData()
        
    }
    
    //TABLE VIEW INITIALIZATION
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return SubjectsViewController.loadedSubjects.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        loadedSubject = SubjectsViewController.loadedSubjects[indexPath.row]
        if DataProvider.sharedInstance.isInternetAvailable() == true
        {
            DataProvider.sharedInstance.getUserEvents(userId: user.id, completion:
            {
                (result) in
                if result == true
                {
                    self.performSegue(withIdentifier: "eventScreenSegue", sender: self)
                }
            })
        }
        else
        {
            self.performSegue(withIdentifier: "eventScreenSegue", sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 62.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubjectCell", for: indexPath) as! SubjectTableViewCell
        
        cell.subjectNameLabel.text = SubjectsViewController.loadedSubjects[indexPath.row].name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            if isInternetAvailable() == true
            {
                DataProvider.sharedInstance.deleteUserSubject(subjectId: SubjectsViewController.loadedSubjects[indexPath.row].id, completion:
                {
                    (result) in
                    if result == true
                    {
                        SubjectsViewController.loadedSubjects.remove(at: indexPath.row)
                        self.subjectsTableVIew.deleteRows(at: [indexPath], with: .fade)
                    }
                    else
                    {
                        let noInternetAlert = UIAlertController(title: "Chyba!", message: "Vymazanie predmetu sa nepodarilo!", preferredStyle: .alert)
                        noInternetAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(noInternetAlert, animated: true, completion: nil)
                    }
                })
            }
            else
            {
                let noInternetAlert = UIAlertController(title: "Chýba pripojenie!", message: "Pre vytvorenie predmetu sa prosím pripojte na internet!", preferredStyle: .alert)
                noInternetAlert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(noInternetAlert, animated: true, completion: nil)
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "eventScreenSegue"
        {
            EventsViewController.loadedEvents.removeAll()
            for event in allUserEvents
            {
                if event.userId == user.id && event.subjectId == loadedSubject?.id
                {
                    EventsViewController.loadedEvents.append(event)
                }
            }
        }
    }
    
    @IBAction func showLeftMenuAction(_ sender: Any)
    {
        leftMenuCall()
    }
    
    //LEFT MENU ACCESS
    func leftMenuCall()
    {
        if self.navigationView.center.x == defaultViewPosition
        {
            UIView.animate(withDuration: 0.3, animations: (
            {
                for view in self.frontViews
                {
                    view.center = CGPoint(x: view.center.x + 188, y: view.center.y)
                }
            }))
        }
        else
        {
            UIView.animate(withDuration: 0.3, animations: (
            {
                for view in self.frontViews
                {
                    view.center = CGPoint(x: self.defaultViewPosition, y: view.center.y)
                }
            }))
        }
    }

    //LEFT MENU DELEGATION
    func logout()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    ////UTILS
    
    //CHECK INTERNET AVAILABILITY
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress)
        {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1)
            {
                zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags)
        {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

}
