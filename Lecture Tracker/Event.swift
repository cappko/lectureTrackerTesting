//
//  Event.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import Foundation
import RealmSwift

var allUserEvents: [Event] = []

class Event: Object
{
    dynamic var id = 0
    dynamic var userId = 0
    dynamic var subjectId = 0
    dynamic var day = ""
    dynamic var room = ""
    dynamic var type = ""
    dynamic var timeOfStart = ""
    dynamic var timeOfEnd = ""

    override static func primaryKey() -> String?
    {
        return "id"
    }
    
}
