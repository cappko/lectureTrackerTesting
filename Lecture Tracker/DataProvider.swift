//
//  DataProvider.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SystemConfiguration
import RealmSwift

class DataProvider
{
    var baseUrl = ""
    
    init()
    {
        //URL BASED ON SCHEMA WE'RE WORKING ON - DEBUG OR RELEASE
        baseUrl = URLConfig.Server.BASEURL
    }
    
    //SINGLE INSTANCE FOR THE WHOLE APP
    class var sharedInstance: DataProvider
    {
        struct Singleton
        {
            static let instance = DataProvider()
        }
        return Singleton.instance
    }
    
    
    //////API METHODS
    
    ////USERS
    
    //LOGIN - POST
    func postUserLogin(email: String, password: String, completion: ((_ result: Bool?) -> Void)!)
    {
        let json: Parameters =
        [
            "email": email,
            "password": password
        ]
        let URL = baseUrl + "/login"
        
        Alamofire.request(URL, method: .post, parameters: json, encoding: JSONEncoding.default).debugLog().responseJSON
        {
            (response) in
            switch response.result
            {
                case .failure(let error):
                    print("ERROR: \(error)")
                    completion?(false)
                    break
                default:
                    break
            }
            
            if let apiResponse = response.result.value as? Dictionary<String, Any>
            {
                let realm = try! Realm()
                let users = realm.objects(User.self)
                
                if users.count > 0
                {
                    for user in users
                    {
                        try! realm.write
                        {
                            realm.delete(user)
                        }
                    }
                }
                
                let statusCode = apiResponse["status"] as! Int
                switch statusCode
                {
                    case 200:
                        let newUser = User()
                        newUser.email = email
                        newUser.password = password
                        newUser.id = apiResponse["user_id"] as! Int

                        try! realm.write
                        {
                            realm.add(newUser, update: true)
                        }
                        
                        user = newUser
                        
                        completion?(true)
                    case 400, 401, 402, 403, 404, 500, 501, 502, 503:
                        completion?(false)
                    default:
                        break
                }
            }
        }
    }
    
    //REGISTRATION - POST
    func postUserRegister(email: String, password: String, completion: ((_ result: String?) -> Void)!)
    {
        let json: Parameters =
        [
            "email": email,
            "password": password,
        ]
        let URL = baseUrl + "/register"
        
        Alamofire.request(URL, method: .post, parameters: json, encoding: JSONEncoding.default).debugLog().responseJSON
        {
            (response) in
            switch response.result
            {
                case .failure(let error):
                    print("ERROR: \(error)")
                    break
                default:
                    break
            }
            
            if let apiResponse = response.result.value as? Dictionary<String, Any>
            {
                print(apiResponse)
                let statusCode = apiResponse["status"] as! Int
                switch statusCode
                {
                    case 200:
                        completion?("true")
                    case 403:
                        completion?("exists")
                    case 400, 401, 402, 404, 500, 501, 502, 503:
                        completion?("false")
                    default:
                        break
                }
            }
        }
    }
    
    ////SUBJECTS

    //NEW SUBJECT - POST
    func postUserSubject(userId: Int, name: String, completion: ((_ result: Int?) -> Void)!)
    {
        let json: Parameters =
        [
            "userId": userId,
            "name": name
        ]
        let URL = baseUrl + "/subjects/\(userId)"
        
        Alamofire.request(URL, method: .post, parameters: json, encoding: JSONEncoding.default).debugLog().responseJSON
        {
            (response) in
            switch response.result
            {
                case .failure(let error):
                    print("ERROR: \(error)")
                    break
                default:
                    break
            }
            
            if let apiResponse = response.result.value as? Dictionary<String, Any>
            {
                print(apiResponse)
                let statusCode = apiResponse["status"] as! Int
                switch statusCode
                {
                    case 200:
                        let subject_id: Int = apiResponse["subjectId"] as! Int
                        completion?(subject_id)
                    case 400, 401, 402, 403, 404, 500, 501, 502, 503:
                        completion?(-1)
                    default:
                        break
                }
            }
        }
    }
    
    //DELETE SUBJECT - DELETE
    func deleteUserSubject(subjectId: Int, completion: ((_ result: Bool?) -> Void)!)
    {
        let URL = baseUrl + "/subjects/\(subjectId)"
        
        Alamofire.request(URL, method: .delete, encoding: JSONEncoding.default).debugLog().responseJSON
        {
            (response) in
            switch response.result
            {
            case .failure(let error):
                print("ERROR: \(error)")
                break
            default:
                break
            }
            
            if let apiResponse = response.result.value as? Dictionary<String, Any>
            {
                print(apiResponse)
                let statusCode = apiResponse["status"] as! Int
                switch statusCode
                {
                case 200:
                    completion?(true)
                case 400, 401, 402, 403, 404, 500, 501, 502, 503:
                    completion?(false)
                default:
                    break
                }
            }
        }
    }
    
    //GET USER SUBJECTS - GET
    func getUserSubjects(userId: Int, completion: ((_ result: Bool?) -> Void)!)
    {
        let URL = baseUrl + "/subjects/\(userId)"
        
        Alamofire.request(URL, method: .get, encoding: JSONEncoding.default).debugLog().responseJSON
        {
            (response) in
            switch response.result
            {
            case .failure(let error):
                print("ERROR: \(error)")
                break
            default:
                break
            }
            
            if let apiResponse = response.result.value as? Dictionary<String, Any>
            {
                print(apiResponse)
                let statusCode = apiResponse["status"] as! Int
                switch statusCode
                {
                case 200:
                    if let subjects = apiResponse["subjects"] as? Array<Dictionary<String, Any>>
                    {
                        let realm = try! Realm()
                        let storedSubjects = realm.objects(Subject.self)
                        
                        if storedSubjects.count > 0
                        {
                            for subject in storedSubjects
                            {
                                try! realm.write
                                {
                                    realm.delete(subject)
                                }
                            }
                        }
                        
                        for subj in subjects
                        {
                            let subject = Subject()
                            subject.id = subj["id"] as! Int
                            subject.name = subj["name"] as! String
                            
                            SubjectsViewController.loadedSubjects.append(subject)
                            
                            try! realm.write
                            {
                                realm.add(subject, update: true)
                            }
                        }
                    }
                    completion?(true)
                case 400, 401, 402, 403, 404, 500, 501, 502, 503:
                    completion?(false)
                default:
                    break
                }
            }
        }
    }

    ////STUDENTS
    
    //NEW STUDENT - POST
    func postUserStudent(userId: Int, name: String, surname: String, email: String, isicnumber: String, completion: ((_ result: Int?) -> Void)!)
    {
        let json: Parameters =
        [
            "userId": userId,
            "name": name,
            "surname": surname,
            "email": email,
            "isicnumber": isicnumber
        ]
        let URL = baseUrl + "/students/\(userId)"
        
        Alamofire.request(URL, method: .post, parameters: json, encoding: JSONEncoding.default).debugLog().responseJSON
        {
            (response) in
            switch response.result
            {
                case .failure(let error):
                    print("ERROR: \(error)")
                    break
                default:
                    break
            }
            
            if let apiResponse = response.result.value as? Dictionary<String, Any>
            {
                print(apiResponse)
                let statusCode = apiResponse["status"] as! Int
                switch statusCode
                {
                    case 200:
                        let student_id: Int = apiResponse["studentId"] as! Int
                        completion?(student_id)
                    case 400, 401, 402, 403, 404, 500, 501, 502, 503:
                        completion?(-1)
                    default:
                        break
                }
            }
        }
    }

    //DELETE STUDENT - DELETE
    func deleteUserStudent(studentId: Int, completion: ((_ result: Bool?) -> Void)!)
    {
        let URL = baseUrl + "/students/\(studentId)"
        
        Alamofire.request(URL, method: .delete, encoding: JSONEncoding.default).debugLog().responseJSON
        {
            (response) in
            switch response.result
            {
                case .failure(let error):
                    print("ERROR: \(error)")
                    break
                default:
                    break
            }
            
            if let apiResponse = response.result.value as? Dictionary<String, Any>
            {
                print(apiResponse)
                let statusCode = apiResponse["status"] as! Int
                switch statusCode
                {
                    case 200:
                        completion?(true)
                    case 400, 401, 402, 403, 404, 500, 501, 502, 503:
                        completion?(false)
                    default:
                        break
                }
            }
        }
    }

    //GET USER STUDENTS - GET
    func getUserStudents(userId: Int, completion: ((_ result: Bool?) -> Void)!)
    {
        let URL = baseUrl + "/students/\(userId)"
        
        Alamofire.request(URL, method: .get, encoding: JSONEncoding.default).debugLog().responseJSON
        {
            (response) in
            switch response.result
            {
                case .failure(let error):
                    print("ERROR: \(error)")
                    break
                default:
                    break
            }
            
            if let apiResponse = response.result.value as? Dictionary<String, Any>
            {
                print(apiResponse)
                let statusCode = apiResponse["status"] as! Int
                switch statusCode
                {
                    case 200:
                        if let students = apiResponse["students"] as? Array<Dictionary<String, Any>>
                        {
                            StudentsViewController.loadedStudents.removeAll()
                            
                            let realm = try! Realm()
                            
                            let storedStudents = realm.objects(Student.self)
                            
                            if storedStudents.count > 0
                            {
                                for student in storedStudents
                                {
                                    try! realm.write
                                    {
                                        realm.delete(student)
                                    }
                                }
                            }
                            
                            for stud in students
                            {
                                let student = Student()
                                student.id = stud["id"] as! Int
                                student.name = stud["name"] as! String
                                student.surname = stud["surname"] as! String
                                student.email = stud["email"] as! String
                                student.isicNumber = stud["isicnumber"] as! String
                                
                                StudentsViewController.loadedStudents.append(student)
                                
                                try! realm.write
                                {
                                    realm.add(student, update: true)
                                }
                            }
                        }
                        
                        completion?(true)
                    case 400, 401, 402, 403, 404, 500, 501, 502, 503:
                        completion?(false)
                    default:
                        break
                }
            }
        }
    }
    
    ////EVENTS
    
    //NEW EVENT - POST
    func postUserEvent(userId: Int, subject_id: Int, day: String, room: String, type: String, timeOfStart: String, timeOfEnd: String, completion: ((_ result: Int?) -> Void)!)
    {
        let json: Parameters =
        [
            "userId": userId,
            "subject_id": subject_id,
            "day": day,
            "room": room,
            "type": type,
            "timeOfStart": timeOfStart,
            "timeOfEnd": timeOfEnd
        ]
        let URL = baseUrl + "/events/\(userId)"
        
        Alamofire.request(URL, method: .post, parameters: json, encoding: JSONEncoding.default).debugLog().responseJSON
        {
            (response) in
            switch response.result
            {
                case .failure(let error):
                    print("ERROR: \(error)")
                    break
                default:
                    break
            }
            
            if let apiResponse = response.result.value as? Dictionary<String, Any>
            {
                let statusCode = apiResponse["status"] as! Int
                switch statusCode
                {
                    case 200:
                        let event_id: Int = apiResponse["eventId"] as! Int
                        completion?(event_id)
                    case 400, 401, 402, 403, 404, 500, 501, 502, 503:
                        completion?(-1)
                    default:
                        break
                }
            }
        }
    }
    
    //DELETE EVENT - DELETE
    func deleteUserEvent(eventId: Int, completion: ((_ result: Bool?) -> Void)!)
    {
        let URL = baseUrl + "/events/\(eventId)"
        
        Alamofire.request(URL, method: .delete, encoding: JSONEncoding.default).debugLog().responseJSON
        {
            (response) in
            switch response.result
            {
                case .failure(let error):
                    print("ERROR: \(error)")
                    break
                default:
                    break
            }
            
            if let apiResponse = response.result.value as? Dictionary<String, Any>
            {
                print(apiResponse)
                let statusCode = apiResponse["status"] as! Int
                switch statusCode
                {
                    case 200:
                        completion?(true)
                    case 400, 401, 402, 403, 404, 500, 501, 502, 503:
                        completion?(false)
                    default:
                        break
                }
            }
        }
    }

    
    //GET USER EVENTS - GET
    func getUserEvents(userId: Int, completion: ((_ result: Bool?) -> Void)!)
    {
        let URL = baseUrl + "/events/\(userId)"
        
        Alamofire.request(URL, method: .get, encoding: JSONEncoding.default).debugLog().responseJSON
        {
            (response) in
            switch response.result
            {
                case .failure(let error):
                    print("ERROR: \(error)")
                    break
                default:
                    break
            }
            
            if let apiResponse = response.result.value as? Dictionary<String, Any>
            {
                print(apiResponse)
                let statusCode = apiResponse["status"] as! Int
                switch statusCode
                {
                    case 200:
                        if let events = apiResponse["events"] as? Array<Dictionary<String, Any>>
                        {
                            allUserEvents.removeAll()
                            EventsViewController.loadedEvents.removeAll()
                            
                            let realm = try! Realm()
                            
                            let storedEvents = realm.objects(Event.self)
                            
                            if storedEvents.count > 0
                            {
                                for event in storedEvents
                                {
                                    try! realm.write
                                    {
                                        realm.delete(event)
                                    }
                                }
                            }
                            
                            for evnt in events
                            {
                                let event = Event()
                                event.id = evnt["id"] as! Int
                                event.userId = user.id
                                event.subjectId = evnt["subject_id"] as! Int
                                event.day = evnt["day"] as! String
                                event.room = evnt["room"] as! String
                                event.type = evnt["type"] as! String
                                event.timeOfStart = evnt["timeOfStart"] as! String
                                event.timeOfEnd = evnt["timeOfEnd"] as! String
                                
                                allUserEvents.append(event)
                                
                                try! realm.write
                                {
                                    realm.add(event)
                                }
                            }
                        }
                        completion?(true)
                    case 400, 401, 402, 403, 404, 500, 501, 502, 503:
                        completion?(false)
                    default:
                        break
                }
            }
        }
    }
    
    ////ATTENDANCE
    
    //NEW ATTENDANCE - POST
    func postAttendance(attendance_id: Int, student_id: Int, event_id: Int, attendance: [Int], completion: ((_ result: Int?) -> Void)!)
    {
        let json: Parameters =
        [
            "attendance_id": attendance_id,
            "student_id": student_id,
            "event_id": event_id,
            "attendance": attendance
        ]
        let URL = baseUrl + "/attendance/\(student_id)"
        
        Alamofire.request(URL, method: .post, parameters: json, encoding: JSONEncoding.default).debugLog().responseJSON
        {
            (response) in
            switch response.result
            {
                case .failure(let error):
                    print("ERROR: \(error)")
                    break
                default:
                    break
            }
            
            if let apiResponse = response.result.value as? Dictionary<String, Any>
            {
                print(apiResponse)
                let statusCode = apiResponse["status"] as! Int
                switch statusCode
                {
                    case 200:
                        if let attendanceId = apiResponse["attendanceId"] as? Int
                        {
                            completion?(attendanceId)
                        }
                    case 400, 401, 402, 403, 404, 500, 501, 502, 503:
                        completion?(-1)
                    default:
                        break
                }
            }
        }
    }
    
    //DELETE ATTENDANCE - DELETE
    func deleteStudentAttendance(attendanceId: Int, completion: ((_ result: Bool?) -> Void)!)
    {
        let URL = baseUrl + "/attendance/\(attendanceId)"
        
        Alamofire.request(URL, method: .delete, encoding: JSONEncoding.default).debugLog().responseJSON
        {
            (response) in
            switch response.result
            {
                case .failure(let error):
                    print("ERROR: \(error)")
                    break
                default:
                    break
            }
            
            if let apiResponse = response.result.value as? Dictionary<String, Any>
            {
                print(apiResponse)
                let statusCode = apiResponse["status"] as! Int
                switch statusCode
                {
                    case 200:
                        completion?(true)
                    case 400, 401, 402, 403, 404, 500, 501, 502, 503:
                        completion?(false)
                    default:
                        break
                }
            }
        }
    }
    
    //GET USER ATTENDANCE - GET
    func getStudentAttendance(studentId: Int, completion: ((_ result: Bool?) -> Void)!)
    {
        let URL = baseUrl + "/attendance/\(studentId)"
        
        Alamofire.request(URL, method: .get, encoding: JSONEncoding.default).debugLog().responseJSON
        {
            (response) in
            switch response.result
            {
                case .failure(let error):
                    print("ERROR: \(error)")
                    completion?(false)
                    break
                default:
                    break
            }
            
            if let apiResponse = response.result.value as? Dictionary<String, Any>
            {
                print(apiResponse)
                let statusCode = apiResponse["status"] as! Int
                switch statusCode
                {
                    case 200:
                        let realm = try! Realm()
                        
                        let allAttendees = realm.objects(Attendance.self)
                        
                        if allAttendees.count > 0
                        {
                            for attendee in allAttendees
                            {
                                try! realm.write
                                {
                                    realm.delete(attendee)
                                }
                            }
                        }
                        AttendeesTableViewController.loadedAttendees.removeAll()
                        
                        if let attendances = apiResponse["attendance"] as? Array<Dictionary<String, Any>>
                        {

                            
                            for i in 0..<attendances.count
                            {
                                let attendance = Attendance()
                                attendance.id = attendances[i]["id"] as! Int
                                attendance.studentId = attendances[i]["student_id"] as! Int
                                attendance.eventId = attendances[i]["event_id"] as! Int
                                let attendanceNumbers = attendances[i]["attendance"] as! Array<Int>
                                
                                let attendanceRealmNumbers = List<Integer>()
                                
                                for attendanceNumber in attendanceNumbers
                                {
                                    let integer = Integer()
                                    integer.val = attendanceNumber
                                    attendanceRealmNumbers.append(integer)
                                }
                                
                                attendance.attendance = attendanceRealmNumbers
                                
                                AttendeesTableViewController.loadedAttendees.append(attendance)
                                try! realm.write
                                {
                                    realm.add(attendance, update: true)
                                }
                            }
                        }
                        completion?(true)
                    case 400, 401, 402, 403, 404, 500, 501, 502, 503:
                        completion?(false)
                    default:
                        break
                }
            }
        }
    }
    
    //GET USER ATTENDANCE BY EVENT ID - GET
    func getStudentAttendanceByEventId(eventId: Int, completion: ((_ result: Bool?) -> Void)!)
    {
        let URL = baseUrl + "/attendanceByEventId/\(eventId)"
        
        Alamofire.request(URL, method: .get, encoding: JSONEncoding.default).debugLog().responseJSON
        {
            (response) in
            switch response.result
            {
                case .failure(let error):
                    print("ERROR: \(error)")
                    completion?(false)
                    break
                default:
                    break
            }
            
            if let apiResponse = response.result.value as? Dictionary<String, Any>
            {
                print(apiResponse)
                let statusCode = apiResponse["status"] as! Int
                switch statusCode
                {
                    case 200:
                        AttendeesTableViewController.loadedAttendees.removeAll()
                        
                        if let attendances = apiResponse["attendance"] as? Array<Dictionary<String, Any>>
                        {
                            for i in 0..<attendances.count
                            {
                                let attendance = Attendance()
                                attendance.id = attendances[i]["id"] as! Int
                                attendance.studentId = attendances[i]["student_id"] as! Int
                                attendance.eventId = attendances[i]["event_id"] as! Int
                                
                                let attendanceNumbers = attendances[i]["attendance"] as! Array<Int>
                                
                                let attendanceRealmNumbers = List<Integer>()
                                
                                for attendanceNumber in attendanceNumbers
                                {
                                    let integer = Integer()
                                    integer.val = attendanceNumber
                                    attendanceRealmNumbers.append(integer)
                                }
                                
                                attendance.attendance = attendanceRealmNumbers
                                
                                AttendeesTableViewController.loadedAttendees.append(attendance)
                                
                                let realm = try! Realm()
                                
                                try! realm.write
                                {
                                    realm.add(attendance, update: true)
                                }
                            }
                        }
                        completion?(true)
                    case 400, 401, 402, 403, 404, 500, 501, 502, 503:
                        completion?(false)
                    default:
                        break
                }
            }
        }
    }

    ////ASSIGNMENTS
    
    //NEW ASSIGNMENT - POST
    func postAssignment(assignment_id: Int, event_id: Int, name: String, student_ids: [Int], student_grade: Int, student_comment: String, student_performance: String, minpoints: Int, maxpoints: Int, completion: ((_ result: Int?) -> Void)!)
    {
        let json: Parameters =
        [
            "assignment_id": assignment_id,
            "event_id": event_id,
            "name": name,
            "student_ids": student_ids,
            "student_grade": student_grade,
            "student_comment": student_comment,
            "student_performance": "Priemerne",
            "minpoints": minpoints,
            "maxpoints": maxpoints
        ]
        let URL = baseUrl + "/assignments/\(student_ids.first!)"
        
        Alamofire.request(URL, method: .post, parameters: json, encoding: JSONEncoding.default).debugLog().responseJSON
        {
            (response) in
            switch response.result
            {
                case .failure(let error):
                    print("ERROR: \(error)")
                    break
                default:
                    break
            }
            
            if let apiResponse = response.result.value as? Dictionary<String, Any>
            {
                print(apiResponse)
                let statusCode = apiResponse["status"] as! Int
                switch statusCode
                {
                    case 200:
                        if let assignmentIds = apiResponse["assignmentIds"] as? [Int]
                        {
                            completion?(assignmentIds.first)
                        }
                    case 400, 401, 402, 403, 404, 500, 501, 502, 503:
                        completion?(-1)
                    default:
                        break
                }
            }
        }
    }
    
    //DELETE ASSIGNMENT - DELETE
    func deleteUserAssignment(assignmentId: Int, completion: ((_ result: Bool?) -> Void)!)
    {
        let URL = baseUrl + "/assignments/\(assignmentId)"
        
        Alamofire.request(URL, method: .delete, encoding: JSONEncoding.default).debugLog().responseJSON
        {
            (response) in
            switch response.result
            {
                case .failure(let error):
                    print("ERROR: \(error)")
                    break
                default:
                    break
            }
            
            if let apiResponse = response.result.value as? Dictionary<String, Any>
            {
                print(apiResponse)
                let statusCode = apiResponse["status"] as! Int
                switch statusCode
                {
                    case 200:
                        completion?(true)
                    case 400, 401, 402, 403, 404, 500, 501, 502, 503:
                        completion?(false)
                    default:
                        break
                }
            }
        }
    }
    
    //GET EVENT ASSIGNMENTS - GET
    func getEventAssignments(eventId: Int, completion: ((_ result: Bool?) -> Void)!)
    {
        let URL = baseUrl + "/assignments/\(eventId)"
        
        Alamofire.request(URL, method: .get, encoding: JSONEncoding.default).debugLog().responseJSON
        {
            (response) in
            switch response.result
            {
                case .failure(let error):
                    print("ERROR: \(error)")
                    completion?(false)
                    break
                default:
                    break
            }
            
            if let apiResponse = response.result.value as? Dictionary<String, Any>
            {
                print(apiResponse)
                let statusCode = apiResponse["status"] as! Int
                switch statusCode
                {
                    case 200:
                        AssignmentsViewController.loadedAssignments.removeAll()
                        
                        if let assignments = apiResponse["assignments"] as? Array<Dictionary<String, Any>>
                        {
                            let realm = try! Realm()
                            
                            let storedAssignments = realm.objects(Assignment.self)
                            
                            if storedAssignments.count > 0
                            {
                                for assignment in storedAssignments
                                {
                                    try! realm.write
                                    {
                                        realm.delete(assignment)
                                    }
                                }
                            }
                            
                            for i in 0..<assignments.count
                            {
                                let assignment = Assignment()
                                assignment.id = assignments[i]["id"] as! Int
                                assignment.eventId = assignments[i]["event_id"] as! Int
                                assignment.name = assignments[i]["name"] as! String
                                assignment.studentId = assignments[i]["student_id"] as! Int
                                assignment.studentGrade = assignments[i]["student_grade"] as! Int
                                assignment.studentPerformance = assignments[i]["student_performance"] as! String
                                assignment.minPoints = assignments[i]["minpoints"] as! Int
                                assignment.maxPoints = assignments[i]["maxpoints"] as! Int
                                
                                AssignmentsViewController.loadedAssignments.append(assignment)
                                
                                try! realm.write
                                {
                                    realm.add(assignment, update: true)
                                }
                            }
                        }
                        completion?(true)
                    case 400, 401, 402, 403, 404, 500, 501, 502, 503:
                        completion?(false)
                    default:
                        break
                }
            }
        }
    }


    
    ////UTILS
    
    //CHECK INTERNET AVAILABILITY
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress)
        {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1)
            {
                zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags)
        {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }


    
}

//NETWORK LOGGING
extension Alamofire.Request
{
    public func debugLog() -> Self
    {
        #if DEBUG
            debugPrint("\n\n\n")
            debugPrint("NETWORK LOG: ")
            debugPrint("URL: \(self.request!.url!) ")
            debugPrint("REQUEST TYPE: \(self.request!.httpMethod!) ")
            if self.request!.allHTTPHeaderFields != nil
            {
                debugPrint("HEADER:")
                for (key, header) in self.request!.allHTTPHeaderFields!
                {
                    let clearHeader = header.replacingOccurrences(of: "\\", with: "")
                    debugPrint("\(key): \(clearHeader.unescaped)")
                }
            }
            if self.request!.httpBody != nil
            {
                let payloadString = String(data: (self.request!.httpBody)!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! 
                let clearPayload = payloadString.replacingOccurrences(of: "\\", with: "")
                debugPrint("PAYLOAD: \(clearPayload.unescaped), encoding: String.Encoding.utf8.rawValue)!) ")
            }
            if self.response?.statusCode != nil
            {
                debugPrint("STATUS CODE: \(self.response!.statusCode) \n\n")
            }
        #endif
        return self
    }
}

extension String
{
    var unescaped: String
    {
        let entities = ["\0", "\t", "\n", "\r", "\"", "\'", "\\"]
        var current = self
        for entity in entities
        {
            let descriptionCharacters = entity.debugDescription.characters.dropFirst().dropLast()
            let description = String(descriptionCharacters)
            current = current.replacingOccurrences(of: description, with: entity)
        }
        return current
    }
}
