//
//  AttendeeTableViewCell.swift
//  Lecture Tracker
//
//  Created by Vojtech Florko
//  Copyright © 2017 Vojtech Florko. All rights reserved.
//

import UIKit
import RealmSwift

class AttendeeTableViewCell: UITableViewCell
{
    @IBOutlet weak var attendeeNameLabel: UILabel!
    @IBOutlet weak var attendeeSwitch: UISwitch!
    
    var indexPathRow: Int = 0

    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    @IBAction func switchAttendee(_ sender: Any)
    {
        if DataProvider.sharedInstance.isInternetAvailable() == false
        {
            reverseSwitch()
        }
        else if attendeeSwitch.isOn == true
        {
            var attendance: [Int] = []
            for _ in 0..<13
            {
                attendance.append(0)
            }
            DataProvider.sharedInstance.postAttendance(attendance_id: 1, student_id: StudentsViewController.loadedStudents[indexPathRow].id, event_id: (loadedEvent?.id)!, attendance: attendance, completion:
            {
                (attendanceId) in
                if attendanceId == -1
                {
                    self.reverseSwitch()
                }
                else
                {
                    let newAttendance = Attendance()
                    newAttendance.id = attendanceId!
                    newAttendance.eventId = (loadedEvent?.id)!
                    newAttendance.studentId = StudentsViewController.loadedStudents[self.indexPathRow].id
                    
                    let realmAttendance = List<Integer>()
                    
                    for attendanceNumber in attendance
                    {
                        let integer = Integer()
                        integer.val = attendanceNumber
                        realmAttendance.append(integer)
                    }
                    
                    newAttendance.attendance = realmAttendance
                    
                    AttendeesTableViewController.loadedAttendees.append(newAttendance)
                    let realm = try! Realm()
                    
                    try! realm.write
                    {
                        realm.add(newAttendance, update: true)
                    }
                }
            })
        }
        else if attendeeSwitch.isOn == false
        {
            var attendeeId = 1
            
            for attendee in AttendeesTableViewController.loadedAttendees
            {
                if attendee.studentId == StudentsViewController.loadedStudents[indexPathRow].id && attendee.eventId == loadedEvent?.id
                {
                    attendeeId = attendee.id
                }
            }
            
            DataProvider.sharedInstance.deleteStudentAttendance(attendanceId: attendeeId, completion:
            {
                (result) in
                if result == false
                {
                    self.reverseSwitch()
                }
                else
                {
                    for i in 0..<AttendeesTableViewController.loadedAttendees.count
                    {
                        if AttendeesTableViewController.loadedAttendees[i].id == attendeeId
                        {
                            AttendeesTableViewController.loadedAttendees.remove(at: i)
                            
                            break
                        }
                    }
                }
            })
        }
    }
    
    func reverseSwitch()
    {
        if attendeeSwitch.isOn == false
        {
            attendeeSwitch.setOn(true, animated: true)
        }
        else
        {
            attendeeSwitch.setOn(false, animated: true)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }

}
